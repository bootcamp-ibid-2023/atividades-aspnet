﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Calculadora.aspx.cs" Inherits="Projeto_Web_Forms_Bootcamp_2023.Calculadora" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Calculadora</title>

    <style>
    body {
        font-family: Arial, sans-serif;
        background-color: #f0f0f0;
        padding: 10px;
        display: flex;
        justify-content: center;
        align-items: center;
        height: 100vh;
        margin: 0;
    }
    .calculadora {
        background-color: #00FF7F;
        padding: 10px;
        border-radius: 5px;
        box-shadow: 10px 100px 20px 0px rgba(0,0,0,0.15);
        width: 50%; 
        max-width: 500px; 
    }
    .calculadora h1 {
        text-align: center;
        margin-bottom: 20px;
    }
    .calculadora input[type="text"], .calculadora_select {
        width: 90%;
        padding: 10px;
        margin-bottom: 25px;
        border-radius: 3px;
        border: 3px solid #ccc;
    }
    .calculadora_button {
        width: 100%;
        padding: 10px;
        border-radius: 5px;
        border: none;
        color: #000000;
        background-color:#FFFFFF;
    }
    .calculadora_button:hover {
        background-color: #7B68EE;
    }
</style>

</head>
<body>
    <form id="form1" runat="server">
        <div class="calculadora">
            <h1>CALCULADORA</h1>
            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
            <asp:DropDownList ID="DropDownList1" runat="server" CssClass="calculadora_select">
                <asp:ListItem>+</asp:ListItem>
                <asp:ListItem>-</asp:ListItem>
                <asp:ListItem>*</asp:ListItem>
                <asp:ListItem>/</asp:ListItem>
            </asp:DropDownList>
            <asp:Button ID="Button1" runat="server" Text="Calcular" CssClass="calculadora_button" OnClick="Button1_Click" />
            <asp:Label ID="Label1" runat="server"></asp:Label>
        </div>
    </form>
</body>
</html>