﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DiasDaSemana.aspx.cs" Inherits="Projeto_Web_Forms_Bootcamp_2023.DiasDaSemana" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Disponibilidade</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>Selecione os dias em que você estará disponível para trabalhar:</h1>
            <asp:CheckBoxList ID="Dias" runat="server">
                <asp:ListItem>Segunda-feira</asp:ListItem>
                <asp:ListItem>Terça-feira</asp:ListItem>
                <asp:ListItem>Quarta-feira</asp:ListItem>
                <asp:ListItem>Quinta-feira</asp:ListItem>
                <asp:ListItem>Sexta-feira</asp:ListItem>
                <asp:ListItem>Sábado</asp:ListItem>
                <asp:ListItem>Domingo</asp:ListItem>
            </asp:CheckBoxList>
            <asp:Button ID="btnSelecionados" runat="server" Text="Selecionados" OnClick="Button1_Click" />
            <asp:Label ID="lblDiasSelecao" runat="server"></asp:Label>
        </div>
    </form>
</body>
</html>
