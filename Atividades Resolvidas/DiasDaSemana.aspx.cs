﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Projeto_Web_Forms_Bootcamp_2023
{
    public partial class DiasDaSemana : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string DiasSelecionados = "Você selecionou para trabalhar em: ";
            foreach (ListItem item in Dias.Items)
            {
                if (item.Selected)
                {
                    DiasSelecionados += item.Text + ", ";
                }
            }
            lblDiasSelecao.Text = DiasSelecionados.TrimEnd(',', ' ');
        }
    }
}